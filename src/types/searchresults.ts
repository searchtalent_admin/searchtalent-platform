import { JobDetail } from "./jobdetails";

export interface JobResults {
  jobs: JobDetail[];
  total_amount: number;
}

export interface JobResultProps {
  startDate: string;
  jobTitle: string;
  location: string;
  id?: number;
  clientName: string;
  logo?: string;
}

export interface SearchResultsProps {
  searchResults: JobResults | undefined;
}

export interface FoundResultsProps {
  searchResults: JobResults | undefined;
  totalResults: number;
  resultsMobile: JobResults | undefined;
  page: number;
  setPage: (page: number) => void;
}

export interface SearchResultsMobileProps {
  resultsMobile: JobResults | undefined;
  page: number;
  setPage: (page: number) => void;
}

export interface SearchResultsWebProps {
  searchResults: JobResults | undefined;
}
