export interface JobCategoryProps {
  name: string;
  icon: string;
}

export interface AutocompleteModel {
  word: string;
  amount: number;
}

export type JobType =
  | "Vollzeittätigkeit"
  | "Teilzeittätigkeit"
  | "Freiberufliche Tätigkeit"
  | "Midijob"
  | "Minijob";

export type Industry =
  | "Organisation/Projektmanagement"
  | "Bauwesen, Handwerk, Umwelt"
  | "Consulting, Beratung"
  | "Einkauf, Materialwirtschaft"
  | "Finanz- und Rechnungswesen, Controlling, Versicherung"
  | "Forschung/Entwicklung, naturwissenschaftliche Berufe (Chemie, Biologie, etc.), Wissenschaft"
  | "Gesundheit, Medizin, Soziales"
  | "Hotellerie und Gastgewerbe, Tourismus"
  | "IT/Telekommunikation"
  | "Marketing, PR, Werbung, Design/Multimedia"
  | "Personalwesen/Recruitment"
  | "Rechts- und Steuerwesen"
  | "Sekretariat/Office Manangement/Verwaltung"
  | "Technische Berufe (Ingenieure, Konstrukteure, Architekten)"
  | "Transport, Verkehr, Logistik, Lager"
  | "Unternehmensführung/Management"
  | "Vertrieb, Verkauf";

export interface SearchFormValues {
  profession: AutocompleteModel;
  location: AutocompleteModel;
  radius?: number | string;
  jobType?: JobType[];
  remote?: boolean;
  industry?: Industry[];
}

export interface IconProps {
  iconname: string;
}

export interface LogInProps {
  username: string;
  password: string;
}
