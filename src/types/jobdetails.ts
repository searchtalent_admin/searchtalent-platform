export interface JobDetail {
  company: { id: number; name: string; logo_url?: string; url?: string };
  id: number;
  place: string;
  start_date: string;
  title: string;
}

export interface RecruitingProcessImageProps {
  title: string;
  imagePath: string;
}

export interface ApplicationTypeOptionProps {
  onClick: any;
  headerText: string;
  descriptionComponent: React.ReactNode;
  buttonText: string;
  buttonIsPrimary: boolean;
  buttonIcon?: any;
}

export interface LanguageKeyFact {
  kind: "language";
  english_knowledge?: string;
  german_knowledge?: string;
}

export interface TravelAllowanceKeyFact {
  kind: "travelAllowance";
  allowance?: boolean;
  allowance_details?: string;
}

export interface ApplicationSectionProps {
  isRecunitedAvailable?: boolean;
  jobAdId?: number;
  prefilledUserData?: ApplicationFormValues;
  recruiterName?: string;
  recruiterImgURL?: string;
  companyName?: string;
  calendly?: string;
  recruiterEmail?: string;
}

export interface SalaryKeyFact {
  kind: "salary";
  show_salary?: boolean;
  salary?: string;
}

export type FactType =
  | string
  | string[]
  | boolean
  | number
  | LanguageKeyFact
  | SalaryKeyFact
  | TravelAllowanceKeyFact;

export interface KeyFactProps {
  factName: string;
  factValue: FactType;
}
export interface KeyFactsProps {
  key?: string;
  amount_coworkers_company?: number;
  amount_coworkers_team?: number;
  benefits?: string[];
  description?: string;
  health_measurements?: boolean;
  holidays?: number;
  home_office?: boolean;
  industry?: string;
  is_temporary?: boolean;
  job_type?: string;
  languages?: LanguageKeyFact;
  salary_info?: SalaryKeyFact;
  allowance_travel?: TravelAllowanceKeyFact;
  remote?: boolean;
}

export interface RecruiterProps {
  id?: number;
  name?: string;
  img?: string;
  email?: string;
}

export type JobDescriptionProps = {
  description: string;
};

export type JobDescriptionSectionProps = {
  description: string;
  setJobDetailsLoaded?: (jobDetailsLoaded: boolean) => void;
};

export type CompanyProps = {
  id: number;
  name: string;
  url: string;
  logo_url?: string;
};

export type AddressProps = {
  zipCode: string;
  city: string;
  streetNameAndNumber: string;
};

export type JobOverviewProps = {
  title: string;
  company: CompanyProps;
  address: AddressProps;
  jobId: string;
  logoLink?: string;
};

export interface Languages {
  [key: string]: string;
}

export interface Education {
  field: string;
  degree: string;
  location: string;
  tillToday: boolean;
  from?: string;
  to?: string;
}

export interface WorkExperience {
  title: string;
  company: string;
  description: string;
  tillToday: boolean;
  from?: string;
  to?: string;
}

export interface BaseUserInformation {
  firstName: string;
  lastName: string;
  email: string;
  academicTitle?: string;
  phone?: string;
}

export interface ApplicationFormValues {
  baseUserInfo: BaseUserInformation;
  skills?: string[];
  languages?: Languages;
  education?: Education[];
  workExperience?: WorkExperience[];
  dsgvo?: boolean;
  noticePeriod?: string;
  earliestDateToStart: string;
  expectedSalary: string;
}

export interface TimelineEntry {
  subtitle: string;
  description: string;
  from: string | undefined;
  to: string | undefined;
}

export interface CVApplicationValues {
  email: string;
  uploadedCV?: File;
  dsgvo: boolean;
  noticePeriod?: string;
  earliestDateToStart: string;
  expectedSalary: string;
}

export interface UploadFilePropsWithoutDropzone {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  error: string | null;
}

export interface UploadFileProps {
  fileError: boolean;
  onChange: (files: File[]) => void;
  label: string;
  errorMessage: string;
}

export interface ApplicationTypeSelectionProps {
  jobAdId?: number;
  setApplicationSectionShown: (applicationTypeToShow: string) => void;
  prefilledUserData?: ApplicationFormValues;
  setRecunitedId?: (id: number) => void;
  setCandidateId: (id: number) => void;
  openApplication?: boolean;
  calendly?: string;
}

// export interface MatchingInfoProps {
//   fit: string;
//   text: string;
//   matchingScore: number;
//   id: string;
// }

export interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

export interface TabsMenuProps {
  keyFacts: KeyFactsProps;
  description: JobDescriptionProps;
  id: string;
  jobOverview: JobOverviewProps;
  isRecunitedAvailable: boolean;
  matchingScore: string | null;
  storageMatchingScore?: number;
}

export interface MatchingData {
  candidate: string;
  company: string | undefined;
}

export interface RecunitedIntroProps {
  setApplicationSectionShown: (section: string) => void;
  jobAdId?: number;
  companyName?: string;
}
