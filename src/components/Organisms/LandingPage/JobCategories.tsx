import { Grid, Paper, Theme } from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import Title from "../../Atoms/Typography/Title";
import JobCategory from "../../Molecules/LandingPage/JobCategory";

const CATEGORIES = [
  { name: "Information & Technology", icon: "/technology.png" },
  { name: "Engineering", icon: "/engineering.png" },
  { name: "Human Resources (HR)", icon: "/hr.png" },
  { name: "Marketing", icon: "/marketing.png" },
  {
    name: "Sales",
    icon: "/sales.png",
  },
  { name: "Kaufmännischer Bereich", icon: "/kaufbereich.png" },
  { name: "Steuern- und Recht", icon: "/recht.png" },
  { name: "Finanzen und Versicherungen", icon: "/finanzen.png" },
  { name: "Office Management", icon: "/office.png" },
  { name: "Medizin", icon: "/medizin.png" },
  { name: "Pharma", icon: "/pharma.png" },
  { name: "Design", icon: "/design.png" },
];

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: "0.5rem",
    },
  })
);

const JobCategories: React.FC = () => {
  const classes = useStyles();
  return (
    <Grid item container xs={12} justifyContent="center" spacing={2}>
      <Grid item container xs={12} justifyContent="center">
        <Title size="medium" text={"Jobkategorien durchsuchen"} />
      </Grid>
      {CATEGORIES.map((category, index) => {
        return (
          <Grid item xs={3} md={3} lg={3} key={index}>
            <Paper elevation={3} className={classes.paper}>
              <JobCategory name={category.name} icon={category.icon} />
            </Paper>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default JobCategories;
