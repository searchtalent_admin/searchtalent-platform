import { IconButtonProps } from "@mui/material";
import { IconButton } from "@mui/material";
import FilterListIcon from "@mui/icons-material/FilterList";
import React from "react";

interface FilterAndSearchProps extends IconButtonProps {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
}

const FilterIconBtn: React.FC<FilterAndSearchProps> = (props) => {
  const { isOpen, setIsOpen, ...other } = props;

  const handleClick = (e: any) => {
    setIsOpen(!isOpen);
  };

  return (
    <IconButton {...other} aria-label="filter" onClick={handleClick} size="large">
      <FilterListIcon />
    </IconButton>
  );
};

export default FilterIconBtn;
