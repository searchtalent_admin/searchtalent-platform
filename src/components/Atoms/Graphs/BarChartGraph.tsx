import { BarChart, Bar, YAxis, Legend } from "recharts";

const data = [
  {
    name: "data",
    searchtalent: 90,
    du: 70,
    amt: 100,
  },
];
export const BarChartGraph = () => {
  return (
    <BarChart width={250} height={180} data={data}>
      <YAxis domain={[0, 100]} unit="%" />
      <Bar dataKey="searchtalent" fill="#504E63" name="searchtalent" />
      <Bar dataKey="du" fill="#504E63" name="du" />
      <Legend align="right" />
    </BarChart>
  );
};
