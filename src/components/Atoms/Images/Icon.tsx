import React from "react";
import SearchIcon from "@mui/icons-material/Search";
import RoomIcon from "@mui/icons-material/Room";
import { IconProps } from "../../../types/jobportal";

const Icon: React.FC<IconProps> = ({ iconname }) => {
  if (iconname === "profession") {
    return <SearchIcon />;
  } else if (iconname === "location") {
    return <RoomIcon />;
  } else {
    return null;
  }
};

export default Icon;
