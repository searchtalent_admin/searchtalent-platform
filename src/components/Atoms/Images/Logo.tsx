import { Theme } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";
import React from "react";
import _ from "lodash";

interface LogoProps {
  title: string;
  image: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    image: {
      [theme.breakpoints.down("md")]: {
        height: "5rem",
        margin: "1rem",
      },
      [theme.breakpoints.up("md")]: {
        height: "3rem",
      },
      [theme.breakpoints.up("lg")]: {
        height: "4rem",
      },
    },
  })
);

const Logo: React.FC<LogoProps> = (props) => {
  const classes = useStyles();
  const { title, image } = props;

  return <img alt={title} src={image} className={classes.image} />;
};

export default Logo;
