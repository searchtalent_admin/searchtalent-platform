import { Typography } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import * as React from "react";
import { MatchingData } from "../../../../types/jobdetails";

function createData(candidate: string, company: string | undefined) {
  return { candidate, company };
}

const setColor = (row: MatchingData) => {
  let color;
  if (row.company) {
    color = "#C94F44";
  } else {
    color = "black";
  }
  return color;
};

const convertData = (data: MatchingData[]) => {
  const rows = data.map((item) => {
    return createData(item.candidate, item.company);
  });
  return rows;
};

interface BasicTableProps {
  data: MatchingData[] | undefined;
}
export const BasicTable: React.FC<BasicTableProps> = ({ data }) => {
  let rows;
  if (data) {
    rows = convertData(data);
  }

  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow sx={{ backgroundColor: "#F5F5F5" }}>
            <TableCell align="center">
              <Typography style={{ fontWeight: 600 }} variant="h6">
                Du
              </Typography>
            </TableCell>
            <TableCell align="left">
              <Typography style={{ fontWeight: 600 }} variant="h6">
                Searchtalent
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows?.map((row) => (
            <TableRow
              key={row.candidate}
              sx={{
                "&:last-child td, &:last-child th": { border: 0 },
                backgroundColor: "#F5F5F5",
              }}
            >
              <TableCell
                component="th"
                scope="row"
                sx={{ color: setColor(row), backgroundColor: "#F5F5F5" }}
              >
                <Typography>{row.candidate}</Typography>
              </TableCell>
              <TableCell sx={{ color: setColor(row) }} align="right">
                <Typography>{row.company}</Typography>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
