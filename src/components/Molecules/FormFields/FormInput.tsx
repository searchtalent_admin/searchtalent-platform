import { TextField, TextFieldProps } from "@mui/material";
import { FieldProps } from "formik";

const FormInput: React.FC<TextFieldProps & FieldProps> = ({
  field,
  form,
  ...props
}) => {
  return (
    <TextField
      variant="standard"
      {...field}
      {...form}
      {...props}
      fullWidth
      error={Boolean(props.error)}
      helperText={props.error ? props.error : ""}
    />
  );
};

export default FormInput;
