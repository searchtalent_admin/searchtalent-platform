import { FieldProps } from "formik";
import React from "react";
import { InputAdornment, TextField } from "@mui/material";
import Icon from "../../Atoms/Images/Icon";

const TextFormField: React.FC<
  FieldProps & {
    iconname?: string;
    variant?: "filled" | "outlined" | "standard";
  }
> = ({ field, form, iconname, variant = "outlined", ...props }) => {
  return (
    <TextField
      fullWidth
      {...field}
      {...props}
      variant={variant}
      size="medium"
      {...(iconname
        ? {
            InputProps: {
              endAdornment: (
                <InputAdornment position="start">
                  <Icon iconname={iconname} />
                </InputAdornment>
              ),
            },
          }
        : {})}
    />
  );
};

export default TextFormField;
