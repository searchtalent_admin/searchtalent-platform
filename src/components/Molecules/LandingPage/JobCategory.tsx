import { Grid } from "@mui/material";
import { JobCategoryProps } from "../../../types/jobportal";
import Text from "../../Atoms/Typography/Text";

const JobCategory: React.FC<JobCategoryProps> = ({ name, icon }) => {
  return (
    <Grid
      container
      alignContent="center"
      alignItems="center"
      justifyContent="center"
    >
      <Grid item xs={12} md={4} lg={4}>
        <img alt={name} src={icon} />
      </Grid>
      <Grid item container xs={12} md={8} lg={8}>
        <Text text={name} />
      </Grid>
    </Grid>
  );
};

export default JobCategory;
