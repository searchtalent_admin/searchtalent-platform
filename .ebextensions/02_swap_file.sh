#!/bin/bash

SWAPFILE=/var/swapfile
SWAP_MEGABYTES=1024
SWAP_BLOCKS=8M

if [ -f $SWAPFILE ]; then
	echo "Swapfile $SWAPFILE found, assuming already setup"
	exit;
fi

/bin/dd if=/dev/zero of=$SWAPFILE bs=$SWAP_BLOCKS count=$SWAP_MEGABYTES
/bin/chmod 600 $SWAPFILE
/sbin/mkswap $SWAPFILE
/sbin/swapon $SWAPFILE
